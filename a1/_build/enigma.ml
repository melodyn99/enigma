(** Steven Jiang (ssj54),
    Lanchin Na (ln244) *)

(** [index c] is the 0-based index of [c] in the alphabet.
    Requires: [c] is an uppercase letter in A..Z. *)
let index (c:char) : int =
  Char.code c - 65

(** [reindex n] is the character representation of n in the alphabet.
    Requires: [n] is a number between 0..25 inclusive *)
let reindex (n:int) : char =
  Char.chr (n + 65)

(** [map_r_to_l wiring top_letter input_pos] is the left-hand output position
    at which current would appear when current enters at right-hand input
    position [input_pos] to a rotor whose wiring specification is given by
    [wiring].  The orientation of the rotor is given by [top_letter], 
    which is the top letter appearing to the operator in the rotor's 
    present orientation.
    Requires: 
    	[wiring] is a valid wiring specification, 
    	[top_letter] is in A..Z, and 
    	[input_pos] is in 0..25. *)
let map_r_to_l (wiring:string) (top_letter:char) (input_pos:int) : int =
  let itop = index top_letter in
  (index (String.get (wiring) ((itop + input_pos) mod 26)) - itop + 26) mod 26


(** [map_l_to_r] computes the same function as [map_r_to_l], except 
    for current flowing left to right.
    Requires:
      [wiring] is a valid wiring specification, 
    	[top_letter] is in A..Z, and 
    	[input_pos] is in 0..25.  *)
let map_l_to_r (wiring:string) (top_letter:char) (input_pos:int) : int =
  let itop = index top_letter in
  (String.index wiring (reindex ((itop + input_pos) mod 26)) - itop + 26) mod 26

(** [map_refl wiring input_pos] is the output position at which current would 
    appear when current enters at input position [input_pos] to a reflector 
    whose wiring specification is given by [wiring].
    Requires: 
    	[wiring] is a valid reflector specification, and 
      [input_pos] is in 0..25. *)
let map_refl (wiring:string) (input_pos:int) : int =
  map_r_to_l wiring 'A' input_pos

(** [map_plug plugs c] is the letter to which [c] is transformed
    by the plugboard [plugs].
    Requires:
      [plugs] is a valid plugboard, and
      [c] is in A..Z. *)
let rec map_plug (plugs:(char*char) list) (c:char) =
  match plugs with 
  | [] -> c
  | (s, i)::t -> if s = c then i else if i = c then s else map_plug t c

(** [rotor] represents an Enigma rotor. *)
type rotor = {
  wiring : string;
  turnover : char;
}

(** [oriented_rotor] represents a rotor that is installed on the spindle hence 
    has a top letter. *)
type oriented_rotor = {
  rotor : rotor;
  top_letter : char;
}

(** [config] represents the configuration of an Enigma machine. 
    See the assignment handout for a description of the fields. *)
type config = {
  refl : string;
  rotors : oriented_rotor list;
  plugboard : (char*char) list;
}

(** [rchar] is the character representation of c after going through one rotor 
    from right to left. *)
let rchar h c = 
  (reindex (map_r_to_l h.rotor.wiring h.top_letter (index c)))

(** [map_rotors_l_to_r] is the output after [c] is transformed through 
    the given rotor list from right to left. ]
    Requires :
      [rotorlist] is a valid oriented_rotor list, and 
      [c] is a char. *)
let rec map_rotors_r_to_l (rotorlist:oriented_rotor list) (c:char) = 
  match rotorlist with
  | [] -> c
  | h::t -> map_rotors_r_to_l (t) (rchar h c)

(** [lchar] represents the character representation of c after going through 
    one rotor from left to right. *)
let lchar h c = 
  (reindex (map_l_to_r h.rotor.wiring h.top_letter (index c)))

(** [map_rotors_l_to_r] is the output after [c] is transformed through 
    the given rotor list from left to right. ]
    Requires :
      [rotorlist] is a valid oriented_rotor list, and 
      [c] is a char. *)
let rec map_rotors_l_to_r (rotorlist:oriented_rotor list) (c:char) = 
  match rotorlist with
  | [] -> c
  | h::t -> map_rotors_l_to_r (t) (lchar h c)

(** [cipher_char config c] is the letter to which the Enigma machine 
    ciphers input [c] when it is in configuration [config].
    Requires:
      [config] is a valid configuration, and
      [c] is in A..Z. *)
let cipher_char (config:config) (c:char) : char =
  let l_to_rrotors = config.rotors in
  let r_to_lrotors = List.rev config.rotors in
  let plug_trans = (map_plug config.plugboard c) in
  let refl_index = (index (map_rotors_r_to_l r_to_lrotors plug_trans)) in
  let laststep = (reindex (map_refl (config.refl) refl_index)) in
  map_plug config.plugboard (map_rotors_l_to_r (l_to_rrotors) laststep)

(** [check_turnover orotor] is the boolean that represents whether or not
    the oriented rotor [orotor] has a top_letter at its turnover
    Requires:
      [orotor] is a valid oriented_rotor *)
let check_turnover (orotor: oriented_rotor) = 
  orotor.top_letter = orotor.rotor.turnover

(** [find_next top] is the next letter that will be the topletter after
    the stepping from the current letter [top].
    Requires:
      [top] is in A..Z.  *)
let find_next (top: char) : char =
  let original = index top in 
  reindex ((original + 1) mod 26)

(** [incr_topletter orotor] is new oriented_rotor whose top_letter is
    the next top_letter after the rotor steps.
    Requires:
      [orotor] is a valid oriented_rotor. *)
let incr_topletter (orotor: oriented_rotor) : oriented_rotor =
  let tletter = orotor.top_letter in
  let new_orotor = {orotor with top_letter = find_next tletter} in
  new_orotor

(** [make_rotorlist rotors] is the oriented_rotor list that results from
    removing the rightmost rotor and checking all of the rotors to see if
    any of them need to have their topletters incremented.
    Requires:
      [rotors] is a valid oreinted_rotor list. *)
let rec make_rotorlist (rotors: oriented_rotor list) 
    (before_turnover: bool) : oriented_rotor list = 
  match rotors with
  | [] -> []
  | h::t -> if check_turnover h then incr_topletter h :: make_rotorlist t true
    else if before_turnover = true then (incr_topletter h):: make_rotorlist t false
    else h :: make_rotorlist t false

(** [make_complete_ rotorlist rotors] is the oriented_rotor list that results
    from the incremented rightmost rotor (the head) cons the oriented_rotor 
    list that results from make_rotorlist on the tail.
    Requires:
      [rotors] is a valid oreinted_rotor list. *)
let make_complete_rotorlist (rotors: oriented_rotor list) : oriented_rotor list = 
  let rev_rotors = List.rev rotors in
  match rev_rotors with
  | [] -> []
  | h::t -> incr_topletter h :: make_rotorlist t (check_turnover h)

(** [step config] is the new configuration to which the Enigma machine 
    transitions when it steps beginning in configuration [config].
    Requires: [config] is a valid configuration. *)
let step (config:config) : config =
  let new_rotors = List.rev (make_complete_rotorlist (config.rotors)) in
  let new_config = {config with rotors = new_rotors} in 
  new_config

(** [cipher config s] is the string to which [s] enciphers
    when the Enigma machine begins in configuration [config].
    Requires:
      [config] is a valid configuration, and
      [s] contains only uppercase letters.*)
let rec cipher (config:config) (s:string): string =
  let sconfig = step config in
  if s = "" then "" 
  else Char.escaped (cipher_char sconfig (String.get s 0)) ^ (cipher sconfig 
    (String.sub s 1 ((String.length s) -1)))


let hours_worked = [8; 8]
