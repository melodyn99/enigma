open OUnit2
(* If you get an "unbound module" error from the line below,
   it's most likely because you have compiled [enigma.ml]. 
   To do that, run [make build]. *)
open Enigma

(** [make_index_test name input expected_output] constructs an OUnit
    test named [name] that asserts the quality of [expected_output]
    with [index input]. *)
let make_index_test 
    (name : string) 
    (input: char) 
    (expected_output : int) : test = 
  name >:: (fun _ -> 
      (* the [printer] tells OUnit how to convert the output to a string *)
      assert_equal expected_output (index input) ~printer:string_of_int)

let index_tests = [
  make_index_test "index A" 'A' 0;
  make_index_test "index G" 'G' 6;
  make_index_test "index M" 'M' 12;
  make_index_test "index V" 'V' 21;
  make_index_test "index Z" 'Z' 25;
]

let make_maprl_test 
    (name: string)
    (input1: string)
    (input2: char)
    (input3: int)
    (expected_output : int) : test =
  name >:: (fun _ ->
      (* the [printer] tells OUnit how to convert the output to a string *)
      assert_equal expected_output (map_r_to_l input1 input2 input3) ~printer:string_of_int)

let map_rl_tests = [
  make_maprl_test "simple rotor topletter A" "ABCDEFGHIJKLMNOPQRSTUVWXYZ" 'A' 0 0; 
  make_maprl_test "example 1 topletter A" "EKMFLGDQVZNTOWYHXUSPAIBRCJ" 'A' 0 4;
  make_maprl_test "complex rotor topletter A" "BACDEFGHIJKLMNOPQRSTUVWXYZ" 'A' 0 1;
  make_maprl_test "complex rotor topletter B" "BACDEFGHIJKLMNOPQRSTUVWXYZ" 'B' 0 25;
  make_maprl_test "complex rotor topletter C" "BACDEFGHIJKLMNOPQRSTUVWXYZ" 'C' 9 9;
  make_maprl_test "rotor III topletter O" "BDFHJLCPRTXVZNYEIWGAKMUSQO" 'O' 14 17;
  make_maprl_test "rotor III topletter J" "BDFHJLCPRTXVZNYEIWGAKMUSQO" 'K' 25 9;
  make_maprl_test "rotor III topletter Z" "BDFHJLCPRTXVZNYEIWGAKMUSQO" 'Z' 0 15;
]

let make_maplr_test 
    (name: string)
    (input1: string)
    (input2: char)
    (input3: int)
    (expected_output : int) : test =
  name >:: (fun _ ->
      (* the [printer] tells OUnit how to convert the output to a string *)
      assert_equal expected_output (map_l_to_r input1 input2 input3) ~printer:string_of_int)

let map_lr_tests = [
  make_maplr_test "simple rotor topletter A" "ABCDEFGHIJKLMNOPQRSTUVWXYZ" 'A' 0 0;
  make_maplr_test "example 1 topletter A" "EKMFLGDQVZNTOWYHXUSPAIBRCJ" 'A' 0 20;
  make_maplr_test "complex rotor topletter A" "BACDEFGHIJKLMNOPQRSTUVWXYZ" 'A' 0 1;
  make_maplr_test "complex rotor topletter B" "BACDEFGHIJKLMNOPQRSTUVWXYZ" 'B' 0 25;
  make_maplr_test "complex rotor topletter C" "BACDEFGHIJKLMNOPQRSTUVWXYZ" 'C' 9 9;
  make_maplr_test "rotor I topletter F" "EKMFLGDQVZNTOWYHXUSPAIBRCJ" 'F' 10 14;
  make_maplr_test "rotor I topletter Z" "EKMFLGDQVZNTOWYHXUSPAIBRCJ" 'Z' 4 7;
  make_maplr_test "rotor I topletter A" "EKMFLGDQVZNTOWYHXUSPAIBRCJ" 'A' 25 9;
]

let make_refl_test 
    (name: string)
    (input1: string)
    (input2: int)
    (expected_output : int) : test =
  name >:: (fun _ ->
      (* the [printer] tells OUnit how to convert the output to a string *)
      assert_equal expected_output (map_refl input1 input2) ~printer:string_of_int)

let map_refl_tests = [
  make_refl_test "simple rotor" "ABCDEFGHIJKLMNOPQRSTUVWXYZ" 0 0; 
  make_refl_test "reflector B" "YRUHQSLDPXNGOKMIEBFZCWVJAT" 5 18;
  make_refl_test "reflector B" "YRUHQSLDPXNGOKMIEBFZCWVJAT" 18 5;
  make_refl_test "reflector B" "YRUHQSLDPXNGOKMIEBFZCWVJAT" 25 19;
  make_refl_test "reflector B" "YRUHQSLDPXNGOKMIEBFZCWVJAT" 0 24;
  make_refl_test "reflector C" "FVPJIAOYEDRZXWGCTKUQSBNMHL" 10 17;
  make_refl_test "reflector C" "FVPJIAOYEDRZXWGCTKUQSBNMHL" 17 10;
  make_refl_test "reflector C" "FVPJIAOYEDRZXWGCTKUQSBNMHL" 15 2;
  make_refl_test "reflector C" "FVPJIAOYEDRZXWGCTKUQSBNMHL" 25 11;
]

let make_plug_test
    (name: string)
    (input1: (char*char) list)
    (input2: char)
    (expected_output : char) : test =
  name >:: (fun _ ->
      (* the [printer] tells OUnit how to convert the output to a string *)
      assert_equal expected_output (map_plug input1 input2) ~printer:Char.escaped)

let map_plug_tests = [
  make_plug_test "empty list" [] 'A' 'A';
  make_plug_test "non-empty list 1" [('A','Z'); ('X','Y')] 'A' 'Z';
  make_plug_test "non-empty list 1" [('A','Z'); ('X','Y')] 'Z' 'A';
  make_plug_test "non-empty list 2" [('Y','X'); ('Z','A')] 'X' 'Y';
  make_plug_test "non-empty list 2" [('Y','X'); ('Z','A')] 'A' 'Z';
  make_plug_test "non-empty list 3" [('A','Z'); ('X','Y'); ('B','C'); ('J','K')] 'J' 'K';
  make_plug_test "non-empty list 3" [('A','Z'); ('X','Y'); ('B','C'); ('J','K')] 'K' 'J';
  make_plug_test "non-empty list 4" [('A','Z'); ('X','Y'); ('B','C'); ('J','K')] 'L' 'L';
]

let make_cipherchar_test
    (name: string)
    (input1: config)
    (input2: char)
    (expected_output : char) : test =
  name >:: (fun _ ->
      (* the [printer] tells OUnit how to convert the output to a string *)
      assert_equal expected_output (cipher_char input1 input2) ~printer:Char.escaped)

let simpleconfig = {refl="ABCDEFGHIJKLMNOPQRSTUVWXYZ"; rotors=[]; plugboard=[]}
let rotorI = {wiring="EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover='A'}
let rotorII = {wiring="AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover='B'}
let rotorIII = {wiring="BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover='C'}
let orotorI = {rotor=rotorI; top_letter='A'}
let orotorII = {rotor=rotorII; top_letter='A'}
let orotorIII = {rotor=rotorIII; top_letter='A'}

let config1 = {
  refl="YRUHQSLDPXNGOKMIEBFZCWVJAT"; 
  rotors=[orotorI; orotorII; orotorIII]; 
  plugboard=[]
}
let config2 = {
  refl="YRUHQSLDPXNGOKMIEBFZCWVJAT"; 
  rotors=[orotorI; orotorII; orotorIII]; 
  plugboard=[('A', 'G'); ('P', 'U')]
}
let config3 = {
  refl="FVPJIAOYEDRZXWGCTKUQSBNMHL";
  rotors=[orotorII; orotorI; orotorIII];
  plugboard=[('A','Z'); ('X','Y'); ('B','C'); ('J','K')]
}
let config4 = {
  refl="YRUHQSLDPXNGOKMIEBFZCWVJAT";
  rotors=[orotorII; orotorIII; orotorI];
  plugboard=[('A','Z'); ('X','Y'); ('B','D'); ('J','L')]
}

let cipher_char_tests = [
  make_cipherchar_test "simple test" simpleconfig 'A' 'A';
  make_cipherchar_test "example test1 with G" config1 'G' 'P';
  make_cipherchar_test "example test1 with A" config1 'A' 'U';
  make_cipherchar_test "example test2" config2 'A' 'U';
  make_cipherchar_test "example test3 with Z" config3 'Z' 'Y';
  make_cipherchar_test "example test3 with A" config3 'A' 'J';
  make_cipherchar_test "example test4 with X" config4 'X' 'N';
  make_cipherchar_test "example test4 with J" config4 'J' 'E';
]  


let make_step_test
    (name: string)
    (input1: config)
    (expected_output : config) : test =
  name >:: (fun _ ->
      (* the [printer] tells OUnit how to convert the output to a string *)
      assert_equal expected_output (step input1))

let srotorI = {wiring="EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover='Q'}
let srotorII = {wiring="AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover='E'}
let srotorIII = {wiring="BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover='V'}
let sorotorI = {rotor=rotorI; top_letter='K'}
let sorotorII = {rotor=rotorII; top_letter='D'}
let sorotorIII = {rotor=rotorIII; top_letter='O'}

let sconfig1 = 
  {refl = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
   rotors =
     [{rotor = {wiring = "BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover = 'V'};
       top_letter = 'K'};
      {rotor = {wiring = "AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover = 'E'};
       top_letter = 'D'};
      {rotor = {wiring = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover = 'Q'};
       top_letter = 'O'}];
   plugboard = []}

let sconfig2 = 
  {refl = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
   rotors =
     [{rotor = {wiring = "BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover = 'V'};
       top_letter = 'K'};
      {rotor = {wiring = "AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover = 'E'};
       top_letter = 'D'};
      {rotor = {wiring = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover = 'Q'};
       top_letter = 'P'}];
   plugboard = []}

let sconfig3 = 
  {refl = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
   rotors =
     [{rotor = {wiring = "BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover = 'V'};
       top_letter = 'K'};
      {rotor = {wiring = "AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover = 'E'};
       top_letter = 'D'};
      {rotor = {wiring = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover = 'Q'};
       top_letter = 'Q'}];
   plugboard = []}

let sconfig4 = 
  {refl = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
   rotors =
     [{rotor = {wiring = "BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover = 'V'};
       top_letter = 'K'};
      {rotor = {wiring = "AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover = 'E'};
       top_letter = 'E'};
      {rotor = {wiring = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover = 'Q'};
       top_letter = 'R'}];
   plugboard = []}

let sconfig5 = 
  {refl = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
   rotors =
     [{rotor = {wiring = "BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover = 'V'};
       top_letter = 'L'};
      {rotor = {wiring = "AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover = 'E'};
       top_letter = 'F'};
      {rotor = {wiring = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover = 'Q'};
       top_letter = 'S'}];
   plugboard = []}

let sconfig6 = 
  {refl = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
   rotors =
     [{rotor = {wiring = "BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover = 'V'};
       top_letter = 'L'};
      {rotor = {wiring = "AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover = 'E'};
       top_letter = 'F'};
      {rotor = {wiring = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover = 'Q'};
       top_letter = 'T'}];
   plugboard = []}

let sconfig7 = 
  {refl = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
  rotors =
    [{rotor = {wiring = "BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover = 'V'};
       top_letter = 'L'};
      {rotor = {wiring = "AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover = 'E'};
       top_letter = 'F'};
      {rotor = {wiring = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover = 'Q'};
       top_letter = 'U'}];
     plugboard = []}

let step_tests = [
  make_step_test "test1 sconfig1" sconfig1 sconfig2;
  make_step_test "test1 sconfig2" sconfig2 sconfig3;
  make_step_test "test1 sconfig3" sconfig3 sconfig4;
  make_step_test "test1 sconfig4" sconfig4 sconfig5;
  make_step_test "test1 sconfig5" sconfig5 sconfig6;
  make_step_test "test1 sconfig6" sconfig6 sconfig7;
]

let make_cipher_test
    (name: string)
    (input1: config)
    (input2: string)
    (expected_output : string) : test =
  name >:: (fun _ ->
      (* the [printer] tells OUnit how to convert the output to a string *)
      assert_equal expected_output (cipher input1 input2))

let cconfig1 = 
  {refl = "YRUHQSLDPXNGOKMIEBFZCWVJAT";
   rotors =
     [{rotor = {wiring = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover = 'Q'};
       top_letter = 'F'};
      {rotor = {wiring = "AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover = 'E'};
       top_letter = 'U'};
      {rotor = {wiring = "BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover = 'V'};
       top_letter = 'N'}];
   plugboard = [('A', 'Z')]}


let cconfig2 = 
  {refl = "YRUHQSLDPXNGOKMIEBFZCWVJAT";
   rotors =
     [{rotor = {wiring = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover = 'Q'};
       top_letter = 'F'};
      {rotor = {wiring = "AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover = 'E'};
       top_letter = 'U'};
      {rotor = {wiring = "BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover = 'V'};
       top_letter = 'N'}];
   plugboard = [('A', 'B'); ('C', 'E')]
  }

let cconfig3 = 
  {refl = "FVPJIAOYEDRZXWGCTKUQSBNMHL";
   rotors =
     [{rotor = {wiring = "AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover = 'E'};
       top_letter = 'G'};
      {rotor = {wiring = "BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover = 'V'};
       top_letter = 'V'};
      {rotor = {wiring = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover = 'Q'};
       top_letter = 'H'}];
   plugboard = [('G', 'C'); ('S', 'J')]
  }

let cconfig4 =
  {refl = "FVPJIAOYEDRZXWGCTKUQSBNMHL";
   rotors =
     [{rotor = {wiring = "AJDKSIRUXBLHWTMCQGZNPYFVOE"; turnover = 'E'};
       top_letter = 'G'};
      {rotor = {wiring = "BDFHJLCPRTXVZNYEIWGAKMUSQO"; turnover = 'V'};
       top_letter = 'N'};
      {rotor = {wiring = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"; turnover = 'Q'};
       top_letter = 'I'};
      {rotor = {wiring = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; turnover = 'G'};
       top_letter = 'G'}];
   plugboard = [('U', 'N'); ('K', 'J'); ('L', 'M'); ('B', 'N')]
  }


let cipher_tests = [
  make_cipher_test "test cconfig1" cconfig1 "YNGXQ" "OCAML";
  make_cipher_test "test cconfig2" cconfig2 "RXQTSBO" "HIWORLD";
  make_cipher_test "test cconfig3" cconfig3 "SAXLQQABKKEU" "YOUARESOCOOL";
  make_cipher_test "test rev cconfig3" cconfig3 "YOUARESOCOOL" "SAXLQQABKKEU";
  make_cipher_test "test cconfig4, four rotors" cconfig4 "ETREYVMS" "TESTFOUR";
]

let tests =
  "test suite for A1"  >::: List.flatten [
    index_tests;
    map_rl_tests;
    map_lr_tests;
    map_refl_tests;
    map_plug_tests;
    cipher_char_tests;
    step_tests;
    cipher_tests;
  ]

let _ = run_test_tt_main tests
